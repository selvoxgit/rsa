<?php

return [
    'auth_enable'           => env('APP_AUTH_ENABLE', false),
    'auth_allow_register'   => env('APP_AUTH_ALLOW_REGISTER', false),
    'auth_allow_verify'     => env('APP_AUTH_ALLOW_VERIFY', false),
    'items_per_page'        => env('APP_ITEMS_PER_PAGE', 30),
];
