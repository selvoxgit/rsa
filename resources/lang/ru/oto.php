<?php

return [

    /*
    |--------------------------------------------------------------------------
    | OTO Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used OTO for display various messages.
    |
    */

    // OTO > Index
    'rsa_oto_id'    => 'Номер ОТО',
    'manager'       => 'Ф.И.О. Директора',

    'status'        => 'Статус',
    'id'            => 'Номер',
    'name'          => 'Название',
    'address'       => 'Адрес',
    'phone'         => 'Телефон',

    // OTO > Show

];
