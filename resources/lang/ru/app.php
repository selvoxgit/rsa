<?php

return [
    'Search for Operators'  => 'Поиск Операторов',
    'Search for Services'   => 'Поиск Сервисов',

    'Operator ID'           => '№',
    'Service ID'            => '№',
    'Manager'               => 'Директор',
    'Company'               => 'Компания',
    'PSRN'                  => 'ОГРН',
    'Categories'            => 'Категории',
    'Address'               => 'Адрес местонахождения',
    'Phone'                 => 'Телефон',
    'Status'                => 'Статус',
    'From'                  => 'С (ДД.ММ.ГГГГ)',
    'To'                    => 'По (ДД.ММ.ГГГГ)',
    'Expression'            => 'Количество сервисов',
    'Value'                 => 'Значение',
    'Email'                 => 'Адрес эл. почты',

    'Operator'              => 'Информация об Операторе',
    'Accreditation'         => 'Информация об аккредитациях',
    'Services'              => 'Информация о пунктах ТО',
    'Certification'         => 'Информация об аттестации',
    'Inspection'            => 'Информация о проверках ОТО',

    'Greater Then'          => 'Больше чем',
    'Less Then'             => 'Меньше чем',
    'Equal'                 => 'Равно',

    'Search'                => 'Найти',
    'Advanced Search'       => 'Продвинутый Поиск',
    'Reset'                 => 'Сброс',
    'Close'                 => 'Закрыть',

    'N/A'                   => 'Аттестат неопределен',
    'Valid'                 => 'Аттестат действителен',
    'Suspended'             => 'Аттестат приостановлен',
    'Invalidate'            => 'Аттестат аннулирован',

    'Print'                 => 'Печать',
    'Export to Excel'       => 'Экспорт в Excel',
    'Export to CSV'         => 'Экспорт в CSV',
    'Export to PDF'         => 'Экспорт в PDF',
    'Save Query'            => 'Сохранить Запрос',
    'Fast Filter'           => 'Быстрый фильтр',

    'No Data Available'     => 'Нет данных для отображения',
];
