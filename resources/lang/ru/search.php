<?php

return [

    /*
    |--------------------------------------------------------------------------
    |
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'status'    => 'Статус',
    'number'    => 'Номер',
    'shortname' => 'Наименование',
    'address'   => 'Адрес',
    'phone'     => 'Телефон',

    'pagination_button_previous' => 'пред.',
    'pagination_button_next' => 'след.',
];
