<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'                    => 'These credentials do not match our records.',
    'throttle'                  => 'Too many login attempts. Please try again in :seconds seconds.',
    'Login'                     => 'Войти',
    'Register'                  => 'Регистрация',
    'Logout'                    => 'Вход',
    'Name'                      => 'Имя пользователя',
    'E-Mail Address'            => 'Адрес эл. почты',
    'Password'                  => 'Пароль',
    'Confirm Password'          => 'Подтверждение пароля',
    'Remember Me'               => 'Запомнить меня',
    'Forgot Your Password?'     => 'Забыли пароль?',
    'Reset Password'            => 'Сброс пароля',
    'Send Password Reset Link'  => 'Сбросить',
    'reset'                     => 'Ваш пароль был сброшен!',
    'sent'                      => 'Мы отправили вам ссылку для сброса пароля по электронной почте!',
    'token'                     => 'Этот токен сброса пароля недействителен.',
    'user'                      => "Мы не можем найти пользователя с таким адресом электронной почты.",
    'throttled'                 => 'Пожалуйста, подождите, прежде чем повторить попытку.',

];
