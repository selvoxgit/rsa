@extends('layouts.app')

{{-- Header --}}
@section('header')
@endsection

{{-- Article --}}
@section('article')
    @include('operators.search._tabs')
    @include('operators.search._results')
    @include('operators.search._pagination')
    @include('operators.search._modal')
@endsection

{{-- Footer --}}
@section('footer')
@endsection
