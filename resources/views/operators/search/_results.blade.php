<div class="container" id="operators-table">
    <div class="row justify-content-md-center">
        <div class="col">
            <div id="search-result" class="search-result">
                @include('partials._toolbar')
                <table id="search-result-table" class="table table-bordered table-hover table-striped">
                    <thead class="thead-primary-alt">
                        <tr>
                            <th scope="col" class="align-middle">{{ __('app.Status') }}</th>
                            <th scope="col" class="align-middle">{{ __('app.Operator ID') }}</th>
                            <th scope="col" class="align-middle">{{ __('app.Company') }}</th>
                            <th scope="col" class="align-middle">{{ __('app.Address') }}</th>
                            <th scope="col" class="align-middle">{{ __('app.Phone') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if( @isset($operators) )
                            @foreach($operators as $operator)
                                @php if( isset($operator->rsa_id) ) $operator->rsa_id = str_pad($operator->rsa_id, 5, '0', STR_PAD_LEFT); @endphp
                                <tr id="{{ $operator->id }}">
                                    <td scope="row" class="text-center" data-orderable="false"><i class="{{ $operator->status->icon }}"></i></td>
                                    <td class="align-middle">{{ $operator->rsa_id }}</td>
                                    <td class="align-middle">{{ $operator->shortname }}</td>
                                    <td class="align-middle">{{ $operator->address }}</td>
                                    <td class="align-middle">{{ $operator->phone }}</td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="5" class="text-center">
                                    {{ __('app.No data Available') }}
                                </td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
