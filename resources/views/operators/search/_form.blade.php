<div class="tab-pane fade show active" id="operators" role="tabpanel" aria-labelledby="operators-tab">
    <div class="tab-pane-container" style="padding: 30px;">
        <form id="search-oto-query" name="search" method="POST" action="/operators" autocomplete="off">
            @csrf
            <div class="form-group row">
                <div class="col-3">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><span class="text-primary">#</span></span>
                        </div>
                        <input type="number" class="form-control" placeholder="{{ __('app.Operator ID') }}" name="id" maxlength="10" value="@if(@isset($form['id'])){{$form['id']}}@endif">
                    </div>
                </div>
                <div class="col-3">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="far fa-user text-primary"></i></span>
                        </div>
                        <input type="text" class="form-control" placeholder="{{ __('app.Manager') }}" name="manager" value="@if(@isset($form['manager'])){{$form['manager']}}@endif">
                    </div>
                </div>
                <div class="col">
                    <input type="text" class="form-control" placeholder="{{ __('app.Company') }}" name="company" value="@if(@isset($form['company'])){{$form['company']}}@endif">
                </div>
            </div>
            <div class="row form-group">
                <div class="col-2">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-book-open  text-primary"></i></span>
                        </div>
                        <input type="number" class="form-control" placeholder="{{ __('app.PSRN') }}" name="psrn" value="@if(@isset($form['psrn'])){{$form['psrn']}}@endif">
                    </div>
                </div>
                <div class="col-3">
                    <select class="selectpicker show-tick form-control" data-style="bootstrap-select-bordered" multiple title="{{ __('app.Categories') }}" data-actions-box="true" data-selected-text-format="count > 1" name="categories[]">
                        @if( @isset($categories) )
                            @foreach($categories as $category)
                                @if( @isset($form['categories']) && array_search($category->id, $form['categories']) !== false )
                                    <option value="{{ $category->id }}" selected="selected">{{ $category->name }}</option>
                                @else
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endif
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="col">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="far fa-map text-primary"></i></span>
                        </div>
                        <input type="text" class="form-control" placeholder="{{ __('app.Address') }}" name="address" value="@if(@isset($form['address'])){{$form['address']}}@endif">
                    </div>
                </div>
            </div>
            <div class="row row-group">
                <span class="row-group-title">Поиск по статусу и количеству сервисов</span>
                <div class="col">
                    <div class="input-group mb-3">
                        <select class="selectpicker show-tick form-control" data-style="bootstrap-select-bordered" title="{{ __('app.Status') }}" data-actions-box="true" name="status">
                            @if( @isset($statuses) )
                                @foreach($statuses as $status)
                                    @if( @isset($form['status']) && $form['status'] == $status->id ) {
                                        <option value="{{ $status->id }}" selected="selected">{{ __('app.'.$status->name) }}</option>
                                    @else
                                        <option value="{{ $status->id }}">{{ __('app.'.$status->name) }}</option>
                                    @endif
                                @endforeach
                            @endif
                        </select>
                        <input type="text" class="form-control datetimepicker" data-target="#status_date_from" id="status_date_from" name="status_date_from">
                        <div class="input-group-append" data-target="#status_date_from" data-toggle="datetimepicker" data-target-input="nearest">
                            <div class="input-group-text"><i class="fa fa-calendar text-primary"></i></div>
                        </div>
                        <input type="text" class="form-control datetimepicker" data-target="#status_date_to" id="status_date_to" name="status_date_to" value="">
                        <div class="input-group-append" data-target="#status_date_to" data-toggle="datetimepicker" data-target-input="nearest">
                            <div class="input-group-text"><i class="fa fa-calendar text-primary"></i></div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="input-group mb-3">
                        <select class="selectpicker show-tick form-control" data-style="bootstrap-select-bordered" title="{{ __('app.Expression') }}" data-actions-box="true" name="service_count_expression">
                            @if( @isset($form['service_count_expression']) && $form['service_count_expression'] == 'g')
                                <option value="g" selected="selected">{{ __('app.Greater Then') }}</option>
                            @else
                                <option value="g">{{ __('app.Greater Then') }}</option>
                            @endif
                            @if( @isset($form['service_count_expression']) && $form['service_count_expression'] == 'l')
                                <option value="l" selected="selected">{{ __('app.Less Then') }}</option>
                            @else
                                <option value="l">{{ __('app.Less Then') }}</option>
                            @endif

                            @if( @isset($form['service_count_expression']) && $form['service_count_expression'] == 'e')
                                <option value="e" selected="selected">{{ __('app.Equal') }}</option>
                            @else
                                    <option value="e">{{ __('app.Equal') }}</option>
                            @endif
                        </select>
                        <input type="number" class="form-control" placeholder="{{ __('app.Value') }}" name="service_count_value" value="@if(@isset($form['service_count_value'])){{$form['service_count_value']}}@endif">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-auto mr-auto">&nbsp;</div>
                <div class="col-auto">
                    <button class="btn btn-primary" type="submit"><i class="fas fa-search"></i>&nbsp;{{ __('app.Search') }}</button>
                    <a class="btn btn-outline-primary" href="/operators"><i class="fas fa-times"></i>&nbsp;{{ __('app.Reset') }}</a>
                </div>
            </div>
        </form>
    </div>
</div>


@if( @isset($form['status_date_from']) || @isset($form['status_date_to']) )
    <script type="text/javascript">
        @if( @isset($form['status_date_from']) )
            $('#status_date_from').val('{{ $form['status_date_from'] }}');
        @endif

        @if( @isset($form['status_date_to']) )
            $('#status_date_to').val('{{ $form['status_date_to'] }}');
        @endif
    </script>
@endif

