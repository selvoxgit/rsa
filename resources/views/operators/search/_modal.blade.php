<div class="container">
    <div class="row justify-content-md-center">
        <div class="col">
            <div id="search-result" class="search-result">
                <div class="modal fade" id="modal_info" tabindex="-1" role="dialog" aria-labelledby="window_title" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-fluid" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="window_title">Информация об Операторе</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div id="modal_info_data" class="modal-body"></div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary" data-dismiss="modal">{{ __('app.Close') }}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
