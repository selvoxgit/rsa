<div class="container d-print-none">
    <div class="row">
        <div class="col">
            <div id="search-panel" class="search-panel">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active font-weight-bold" id="operators-tab" data-toggle="tab" href="#operators" role="tab" aria-controls="operators" aria-selected="true">
                            {{ __('app.Search for Operators') }}
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="services-tab" href="/services">
                            {{ __('app.Search for Services') }}
                        </a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    @include('operators.search._form')
                </div>
            </div>
        </div>
    </div>
</div>
