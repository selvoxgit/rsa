<div role="tabpanel" id="pto" class="tab-pane fade" aria-labelledby="pto-tab">
    <table class="table table-bordered table-striped">
        <thead class="thead-primary-alt">
        <tr>
            <th>{{ __('app.Status') }}</th>
            <th>{{ __('app.Service ID') }}</th>
            <th>{{ __('app.Address') }}</th>
            <th>{{ __('app.Phone') }}</th>
            <th>{{ __('app.Categories') }}</th>
        </tr>
        </thead>
        <tbody>
        @if( @isset($operator->services) )
            @if( $operator->services->count() )
                @foreach( $operator->services as $service )
                    <tr>
                        <td class="align-middle"><i class="{{ $operator->status->icon }}"></i></td>
                        <td class="align-middle">{{ $service->rsa_id }}</td>
                        <td class="align-middle">{{ $service->address }}</td>
                        <td class="align-middle">{{ $service->phone }}</td>
                        <td class="align-middle">
                            @if( @isset($service->categories) )
                                @foreach( $service->categories as $category)
                                    <button type="button" class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="{{ $category->description }}">
                                        {{ $category->name }}
                                    </button>
                                @endforeach
                            @endif
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="5" class="align-middle text-center">
                        {{ __('app.No Data Available') }}
                    </td>
                </tr>
            @endif
        @endif
        </tbody>
    </table>
</div>
