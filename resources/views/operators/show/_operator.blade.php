@if( @isset($operator) )
    <div class="row">
        <div class="col">

            {{-- Информация об операторе --}}
            <table class="table table-borderless table-sm">
                <tbody>
                <tr>
                    <th>{{ __('app.Operator ID') }}</th>
                    <td class="text-muted">{{ $operator->rsa_id }}</td>
                </tr>
                @if( @isset($operator->fullname) && @strlen($operator->fullname) )
                    <tr>
                        <th>{{ __('app.Company') }}</th>
                        <td class="text-muted">{{ $operator->fullname }}</td>
                    </tr>
                @else
                    @if( @isset($operator->shortname) && @strlen($operator->shortname) )
                        <tr>
                            <th>{{ __('app.Company') }}</th>
                            <td class="text-muted">{{ $operator->shortname }}</td>
                        </tr>
                    @endif
                @endif
                <tr>
                    <th>{{ __('app.Manager') }}</th>
                    <td class="text-muted">{{ $operator->manager }}</td>
                </tr>
                <tr>
                    <th>{{ __('app.PSRN') }}</th>
                    <td class="text-muted">{{ $operator->ogrn }}</td>
                </tr>
                <tr>
                    <th>{{ __('app.Status') }}</th>
                    <td class="text-muted">{{ __('app.'.$operator->status->name) }}</td>
                </tr>
                <tr>
                    <th>{{ __('app.Categories') }}</th>
                    <td class="text-muted">
                        {{--
                            Tooltip?
                            <button type="button" class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="tool tip description">L</button>
                         --}}
                    </td>
                </tr>
                <tr>
                    <th>{{ __('app.Address') }}</th>
                    <td class="text-muted">{{ $operator->address }}</td>
                </tr>
                <tr>
                    <th>{{ __('app.Phone') }}</th>
                    <td class="text-muted">{{ $operator->phone }}</td>
                </tr>
                <tr>
                    <th>{{ __('app.Email') }}</th>
                    <td class="text-muted"><a href="mailto:">{{ $operator->email }}</a></td>
                </tr>
                {{-- @endif --}}
                </tbody>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col">

            <div id="search-panel" class="search-panel">
                <form id="search-query" name="search-query" method="post" action="#">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item"><a class="nav-link active" id="info-accreditation-tab" data-toggle="tab" href="#accreditation" role="tab" aria-controls="accreditation" aria-selected="true">{{ __('app.Accreditation') }}</a></li>
                        <li class="nav-item"><a class="nav-link" id="info-pto-tab" data-toggle="tab" href="#pto" role="tab" aria-controls="pto" aria-selected="false">{{ __('app.Services') }}</a></li>
                        <li class="nav-item"><a class="nav-link" id="info-attestation-tab" data-toggle="tab" href="#attestation" role="tab" aria-controls="attestation" aria-selected="false">{{ __('app.Certification') }}</a></li>
                        <li class="nav-item"><a class="nav-link" id="info-inspections-tab" data-toggle="tab" href="#inspections" role="tab" aria-controls="inspections" aria-selected="false">{{ __('app.Inspection') }}</a></li>
                    </ul>
                    <div class="tab-content p-2" id="myTabContent">

                        {{-- Информация об аккредитациях --}}
                        @include('operators.show._accreditation')

                        {{--  Информация о пунктах ТО --}}
                        @include('operators.show._service')

                        {{--  Информация об аттестации --}}
                        @include('operators.show._certification')

                        {{--  Информация о проверках ОТО --}}
                        @include('operators.show._inspection')

                    </div>
                </form>
            </div>
        </div>
    </div>

@endif
