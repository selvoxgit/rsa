<div role="tabpanel" id="inspections" class="tab-pane fade" aria-labelledby="verification-tab">
    <table class="table table-bordered table-striped">
        <thead class="thead-primary-alt">
        <tr>
            <th>№</th>
            <th>Основание для проведения проверки</th>
            <th>Дата проведения проверки</th>
            <th>Дата Акта/Заключения</th>
            <th>Адрес ПТО</th>
            <th>Выявленные нарушения</th>
            <th>Реквизиты Принятого Решения</th>
            <th>Результат проверки</th>
        </tr>
        </thead>
        <tbody>
        @if( @isset($operator->inspections) )
            @if( $operator->inspections->count() > 0 )
                @foreach( $operator->inspections as $inspection )
                    <tr>
                        <td class="align-middle">{{ $inspection->number }}</td>
                        <td class="align-middle">{{ $inspection->subject }}</td>
                        <td class="align-middle">{{ $inspection->checkdate }}</td>
                        <td class="align-middle">{{ Carbon\Carbon::parse($inspection->actdate)->format("d-m-Y") }}</td>
                        <td class="align-middle">{{ $inspection->service_address }}</td>
                        <td class="align-middle">{{ $inspection->violations }}</td>
                        <td class="align-middle">{{ $inspection->document }}</td>
                        <td class="align-middle">{{ $inspection->result }}</td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="8" class="text-center">
                        {{ __('app.No Data Available') }}
                    </td>
                </tr>
            @endif
        @endif
        </tbody>
    </table>
</div>
