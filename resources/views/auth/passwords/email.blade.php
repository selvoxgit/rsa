@extends('layouts.app')

@section('content')
<div class="container h-100">
    <div class="row d-flex justify-content-center align-self-center h-100">
        <div class="col-lg-6 align-self-center">
            <div class="card">
                <div class="card-header bg-primary text-white">
                    <i class="fas fa-redo"></i>&nbsp;&nbsp;
                    {{ __('auth.Reset Password') }}
                </div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf
                        <div class="form-group row mb-3">
                            <label for="email" class="col-md-5 col-form-label text-md-right">
                                {{ __('auth.E-Mail Address') }}
                            </label>
                            <div class="col-md-7 mb-3">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col col-md-12 text-right">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('auth.Send Password Reset Link') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
