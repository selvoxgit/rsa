@extends('layouts.app')

@section('content')
<div class="container h-100">
    <div class="row d-flex justify-content-center align-self-center h-100">
        <div class="col-lg-6 align-self-center">
            <div class="card border-primary justify-content-center">
            <div class="card">
                <div class="card-header bg-primary text-white">
                    <i class="fas fa-paper-plane"></i>&nbsp;&nbsp;
                    {{ __('auth.Verify Your Email Address') }}
                </div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('auth.A fresh verification link has been sent to your email address.') }}
                        </div>
                    @endif

                    {{ __('auth.Before proceeding, please check your email for a verification link.') }}
                    {{ __('auth.If you did not receive the email') }},
                    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <button type="submit" class="btn btn-link p-0 m-0 align-baseline">{{ __('auth.click here to request another') }}</button>.
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
