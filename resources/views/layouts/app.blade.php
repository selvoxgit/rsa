<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <title>{{ config('app.name', 'Laravel') }}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Styles -->
    <link href="{{ asset('public/css/app.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script src="{{ asset('public/js/app.js') }}"></script>

</head>
<body class="vh-100">
@if( View::hasSection('prefix') )

    <div id="app" class="app @yield('prefix')">
        @endif

        @if( View::hasSection('header') )
            @include('partials.header')
        @endif

        @if(View::hasSection('content'))
            @yield('content');
        @endif

        @if( View::hasSection('article') )
            <article id="article" class="article">
                @yield('article')
            </article>
        @endif

        @if( View::hasSection('footer') )
            @include('partials.footer')
        @endif
    </div>

    <script src="{{ asset('public/js/vendor.js') }}"></script>
</body>
</html>
