<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <title>{{ config('app.name', 'Laravel') }}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="{{ asset('public/js/app.js') }}" defer></script>
    <script src="{{ asset('public/js/f.js') }}" defer></script>
    <script src="{{ asset('public/js/vendor.js') }}" defer></script>
    <link href="{{ asset('public/css/app.css') }}" rel="stylesheet">
</head>
<body class="vh-100">

    @yield('article')

</body>
</html>
