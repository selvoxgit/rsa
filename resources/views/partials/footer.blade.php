<footer id="footer" class="footer mb-3">
    <div class="text-center text-muted text-light">
        &copy; Copyright 2014-@php echo date("Y",time()); @endphp , Selvox Engineering Ltd.
    </div>
</footer>

