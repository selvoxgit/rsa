<div class="container">
    <div class="row">
        <div class="col">
            <div id="search-panel" class="search-panel">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="operators-tab" data-toggle="tab" href="#operators" role="tab" aria-controls="operators" aria-selected="true">{{ __('app.Operators') }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="services-tab" data-toggle="tab" href="#services" role="tab" aria-controls="services" aria-selected="false">{{ __('app.Services') }}</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="operators" role="tabpanel" aria-labelledby="operators-tab">
                        @include('partials.forms.operators')
                    </div>
                    <div class="tab-pane fade" id="services" role="tabpanel" aria-labelledby="services-tab">
                        @include('partials.forms.services')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
