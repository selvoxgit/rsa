{{--
<div id="search-pagination" class="search-pagination">
    <nav aria-label="Page navigation example">
        <ul class="pagination justify-content-center">
            <li class="page-item disabled">
                <a class="page-link" href="#" tabindex="-1" aria-disabled="true">{{ __('search.pagination_button_previous')  }}</a>
            </li>
            <li class="page-item active"><a class="page-link" href="#">1</a></li>
            <li class="page-item"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item">
                <a class="page-link" href="#">{{ __('search.pagination_button_next')  }}</a>
            </li>
        </ul>
    </nav>
</div>
--}}

@if( @isset($operators) )
    <div class="pagination justify-content-center mt-5">
        {{ $operators->links() }}
    </div>
@endif


