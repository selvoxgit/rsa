<div class="d-print-none row">
    <div class="col-8 text-left align-self-center">
        <blockquote class="blockquote font-weight-light" style="margin-bottom: 0 !Important;">

        @if( @isset($items_found) )
        &nbsp;  Найдено: <b>{{ $items_found }}</b> записей</b>
        @endif
        @if( @isset($items_total) )
            из <b>{{ $items_total }}</b>
        @endif
            @if( @isset($parsed) )
                , Последнее бновление: <b>{{ $parsed->created_at }}</b>
            @endif
        </blockquote>
    </div>
    <div class="col text-right">
        <div class="btn-group btn-group-md" role="group" aria-label="Second group">
            <button type="button" class="btn btn-outline-primary" id="toolbar-print" data-toggle="tooltip" data-placement="top" title="{{ __('app.Print') }}"><i class="fas fa-print"></i></button>
 {{--           <button type="button" class="btn btn-outline-primary" id="toolbar-excel" data-toggle="tooltip" data-placement="top" title="{{ __('app.Export to Excel') }}"><i class="far fa-file-excel"></i></button>
            <button type="button" class="btn btn-outline-primary" id="toolbar-csv" data-toggle="tooltip" data-placement="top" title="{{ __('app.Export to CSV') }}"><i class="fas fa-file-csv"></i></button>
            <button type="button" class="btn btn-outline-primary" id="toolbar-pdf" data-toggle="tooltip" data-placement="top" title="{{ __('app.Export to PDF') }}"><i class="far fa-file-pdf"></i></button>
            <button type="button" class="btn btn-outline-primary" id="toolbar-database" data-toggle="tooltip" data-placement="top" title="{{ __('app.Save Query') }}"><i class="fas fa-database"></i></button>
            <button type="button" class="btn btn-outline-primary" id="toolbar-filter" data-toggle="tooltip" data-placement="top" title="{{ __('app.Fast Filter') }}"><i class="fas fa-filter"></i></button> --}}
        </div>
    </div>
</div>
