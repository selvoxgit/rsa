<div role="tabpanel" id="attestation" class="tab-pane fade" aria-labelledby="status-tab">
    <table class="table table-bordered table-striped">
        <thead class="thead-primary-alt">
        <tr>
            <th>Основание Вынесения Решения</th>
            <th>Тип Решения</th>
            <th>Дата Приказа</th>
            <th>№ Приказа</th>
        </tr>
        </thead>
        <tbody>
        @if( @isset($operator->certifications) )
            @if( $operator->certifications->count() )
                @foreach( $operator->certifications as $certification )
                    <tr>
                        <td class="align-middle">{{ $certification->subject }}</td>
                        <td class="align-middle">{{ $certification->type }}</td>
                        <td class="align-middle">{{ $certification->orderdate }}</td>
                        <td class="align-middle">{{ $certification->ordernum }}</td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="4" class="text-center">No Data</td>
                </tr>
            @endif
        @endif
        </tbody>
    </table>
</div>
