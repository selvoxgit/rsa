<div role="tabpanel" id="accreditation" class="tab-pane fade show active" aria-labelledby="accreditation-tab">
    <table class="table table-bordered table-striped">
        <thead class="thead-primary-alt">
        <tr>
            <th>Вид Решения</th>
            <th>Номер Аттестата</th>
            <th>Дата Приказа</th>
            <th>Номер Приказа</th>
            <th>Область Аккредитации</th>
            <th>Кол-во ПТО</th>
        </tr>
        </thead>
        <tbody>
        @if( @isset($operator->accreditations) )
            @if( $operator->accreditations->count() > 0 )
                @foreach( $operator->accreditations as $accreditation )
                    <tr>
                        <td class="align-middle">{{ $accreditation->type }}</td>
                        <td class="align-middle">{{ $accreditation->certnum }}</td>
                        <td class="align-middle">{{ $accreditation->orderdate }}</td>
                        <td class="align-middle">{{ $accreditation->ordernum }}</td>
                        <td class="align-middle">
                            @if( @isset($accreditation->categories) )
                                @foreach( $accreditation->categories as $category )
                                    <button type="button" class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="{{ $category->description }}">
                                        {{ $category->name }}
                                    </button>
                                @endforeach
                            @endif
                        </td>
                        <td class="align-middle">{{ $accreditation->services }}</td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="6" class="text-center">
                        No Data
                    </td>
                </tr>
            @endif
        @endif
        </tbody>
    </table>
</div>
