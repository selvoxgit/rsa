<div class="container" id="operators-table">
    <div class="row justify-content-md-center">
        <div class="col">
            <div id="search-result" class="search-result">
                @include('partials._toolbar')
                <table id="search-result-table" class="table table-bordered table-hover table-striped">
                    <thead class="thead-primary-alt">
                        <tr>
                            <th scope="col" class="align-middle">{{ __('app.Status') }}</th>
                            <th scope="col" class="align-middle">{{ __('app.Operator ID') }}</th>
                            <th scope="col" class="align-middle">{{ __('app.Company') }}</th>
                            <th scope="col" class="align-middle">{{ __('app.Address') }}</th>
                            <th scope="col" class="align-middle">{{ __('app.Phone') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if( @isset($services) )
                            @foreach($services as $service)
                                <tr id="{{ $service->id }}">
                                    <th scope="row" class="text-center" data-orderable="false"><i class="{{ $service->operator->status->icon }}"></i></th>
                                    <td class="align-middle">{{ $service->rsa_id }}</td>
                                    <td class="align-middle">{{ $service->operator->shortname }}</td>
                                    <td class="align-middle">{{ $service->address }}</td>
                                    <td class="align-middle">{{ $service->phone }}</td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
