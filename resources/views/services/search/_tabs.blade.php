<div class="container">
    <div class="row">
        <div class="col">
            <div id="search-panel" class="search-panel">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link" id="operators-tab" href="/operators">
                            {{ __('app.Search for Operators') }}
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active font-weight-bold" id="services-tab" data-toggle="tab" href="#services" role="tab" aria-controls="services" aria-selected="false">
                            {{ __('app.Search for Services') }}
                        </a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="services" role="tabpanel" aria-labelledby="services-tab">
                        @include('services.search._form')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
