@extends('layouts.app')

{{-- Header --}}
@section('header')
@endsection

{{-- Article --}}
@section('article')
    @include('services.search._tabs')
    @include('services.search._results')
    @include('services.search._pagination')
    @include('services.search._modal')
@endsection

{{-- Footer --}}
@section('footer')
@endsection
