<div class="tab-pane-container" style="padding: 30px;">
    <form id="search-oto-query" name="search-pto-query" method="post" action="/services" autocomplete="off">
        @csrf
        <div class="form-group row">
            <div class="col-3">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1"><span class="text-primary">#</span></span>
                    </div>
                    <input type="number" class="form-control" placeholder="{{ __('app.Service ID') }}" name="id" maxlength="10" value="{{ old('id') }}">
                </div>
            </div>
            <div class="col">
                <input type="text" class="form-control" placeholder="{{ __('app.Company') }}" name="company" value="{{ old('company') }}">
            </div>
            <div class="col-3">
                <select class="selectpicker show-tick form-control" data-style="bootstrap-select-bordered" multiple title="{{ __('app.Categories') }}" data-actions-box="true" data-selected-text-format="count > 1" name="categories[]">
                    @if( @isset($categories) )
                        @foreach($categories as $category)
                            @if( @isset($form['categories']) && array_search($category->id, $form['categories']) !== false )
                                <option value="{{ $category->id }}" selected="selected">{{ $category->name }}</option>
                            @else
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endif
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1"><i class="far fa-map text-primary"></i></span>
                    </div>
                    <input type="text" class="form-control" placeholder="{{ __('app.Address') }}" name="address" value="{{ old('address') }}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-auto mr-auto">&nbsp;</div>
            <div class="col-auto">
                <button class="btn btn-primary" type="submit"><i class="fas fa-search"></i>&nbsp;{{ __('app.Search') }}</button>
                <a class="btn btn-outline-primary" href="/services"><i class="fas fa-times"></i>&nbsp;{{ __('app.Reset') }}</a>
            </div>
        </div>
    </form>
</div>
