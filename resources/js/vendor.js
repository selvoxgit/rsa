
$( document ).ready(function(){
    $('select').selectpicker();

    // Initialize tables
    var searchResultTable = $('#search-result-table').DataTable({
        "paging":       false,
        "searching":    false,
        "info":         false,
        "select":       true,
    });

    // Initialize datepicker
    $('.datetimepicker').datetimepicker({
        locale:     'ru',
        format:     'YYYY-MM-DD',
        useCurrent: false,
        useStrict:  true
    });

    // Initialize tooltips
    $('[data-toggle="tooltip"]').tooltip();

    $('#toolbar-print').on('click', function(){
        window.print();
    });

    $('#toolbar-excel').on('click', function(){
        alert('Excel function not available now!')
    });

    $('#toolbar-filter').on('click', function(){
        alert('Filter function not available now!')
    });

    // Ajax for show selected OTO
    $('#search-result-table tbody').on('click', 'tr', function() {
        //var d = searchResultTable.row(this).data();
        var id = searchResultTable.row(this).id();
        //console.log('id: ' + id);
        $.ajax({
            url: '/operator/'+id+'/show',
            type: 'post',
            data: {
                '_token':   $('meta[name="csrf-token"]').attr('content'),
                'id': id
            },
            success: function(response){
                $('#modal_info_data').html(response.msg);
                $('[data-toggle="tooltip"]').tooltip();
                $('#modal_info').modal('show');
            },
            error: function() {
                $('#modal_info_data').html('error');
                $('#modal_info').modal('show');
            }
        })
    });
});
