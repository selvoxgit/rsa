const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// Javascripts
mix.js('resources/js/app.js', 'public/js');
mix.js('resources/js/vendor.js', 'public/js');

// Styles
mix.sass('resources/sass/app.scss', 'public/css');

// Fonts
mix.copy('node_modules/roboto-fontface/fonts/roboto', 'public/fonts/roboto/');
mix.copy('node_modules/@fortawesome/fontawesome-free/webfonts', 'public/fonts/faw/');
mix.copy('node_modules/@mdi/font/fonts', 'public/fonts/mdi/');
mix.copy('resources/images', 'public/images');

// Version
mix.version();

