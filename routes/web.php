<?php

use Illuminate\Support\Facades\Route;

Auth::routes(array(
    'register' => boolval(config('rsa.auth_allow_register', false)),
    'verify'   => boolval(config('rsa.auth_allow_verify', false)),
));

// Поиск и Отображение информации об Операторах
Route::get('/', 'Operators@search');
Route::get('/operators', 'Operators@search');
Route::post('/operators', 'Operators@search');
Route::get('/operator/{id}/show', 'Operators@show');
Route::post('/operator/{id}/show', 'Operators@show');

// Поиск и Отображение информации об Сервисах
Route::get('/services', 'Services@search');
Route::post('/services', 'Services@search');
Route::get('/service/{id}/show', 'Services@show');
Route::post('/service/{id}/show', 'Services@show');
