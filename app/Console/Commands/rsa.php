<?php

namespace App\Console\Commands;

use App\Http\Controllers\RSACreator;
use App\Http\Controllers\Search;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

class rsa extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rsa:parse';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Structural analysis of the RSA site.';

    /**
     * Адрес страницы, которая содержит сведения
     * из реестра операторов технического осмотра.
     */
    const RSA_URL = 'http://oto-register.autoins.ru/oto/index.xhtml';

    /**
     * Класс для работы с cookie.
     *
     * @var CookieJar
     */
    protected $cookies;

    /**
     * Содержание главной страницы OTO_REGISTER_URL.
     */
    protected $html;

    /**
     * @var int Кол-во операторов в базе данных РСА
     */
    private $operator_count;

    /**
     * @var int Последний идентификатор ОТО в базе данных РСА
     */
    private $operator_lastid;

    /**
     * Конструктор класса.
     *
     * @return void
     */
    public function __construct()
    {
        // Инициализация родительского класса
        parent::__construct();

        // Инициализация cookie
        $this->cookies = new \GuzzleHttp\Cookie\CookieJar();

        // Инициализация http слиента
        $this->http = new Client(['cookies'=> $this->cookies]);

        // Кол-во операторов в базе данных РСА
        $this->operator_count = 0;

        // Последний идентификатор ОТО в базе данных РСА
        $this->operator_lastid = 0;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Кол-во операторов в базе данных РСА
        if( ($operator_count = $this->requestOperatorCount()) !== false ) {
            $this->operator_count = $operator_count;
            $this->info("Count of operators in RSA database: $operator_count");
        }

        // Последний идентификатор ОТО в базе данных РСА
        if( ($operator_lastid = $this->requestOperatorLastID()) !== false ) {
            $this->operator_lastid = $operator_lastid;
            $this->info("The last identifier of Company added to the PCA database: $operator_lastid");
        }

        //$this->operator_lastid = 156;
        for( $i=$this->operator_lastid; $i > 0; $i-- ) {
            if( ($operator = $this->requestOperator($i)) !== false ) {
                $this->db_create($operator, $i);
            }
        }
    }

    /**
     * Функция сохраняет информациб об Операторе ТО в базе данных
     *
     * @param $operator Ассоциативный массив
     * @param $id Идентификатор оператора по базе данных РСА
     *
     * @return void
     */
    private function db_create($operator, $id)
    {
        // Отслеживаем ошибки при работе с БД
        try{

            // Начало транзакции
            DB::beginTransaction();

            // Вывод сообщения о начале работы парсинга
            $this->output->write("Processing operator #$id...");

            // Создаем объект для вставки записи в БД
            $creator = new RSACreator();

            // Добавляем оператора в базу данных
            $creator->store($operator);

            // Вывод сообщения в консоль об успешной операции
            $this->output->write("SUCCESS", true);

            // Разрешаем транзакцию
            DB::commit();

        // В случае ошибки БД
        } catch( \Exception $error ) {

            // Отмена транзакции
            DB::rollBack();

            // Вывод сообщения в консоль об ошибке
            $this->output->write("FAIL", true);

            // Выводим дополнительную информацию об ошибке в консоль
            //$this->warn("Error: ".$error->getMessage()."\n");

            // Сохраняем дополнительную информацию об ошибке в лог-файл rsa.log
            Log::channel('rsa')->error("Error: ".$error->getMessage()."\n");
        }
    }

    /**
     * @param $id
     * @param $operator
     */
    private function db_update($id, $operator)
    {

    }

    /**
     *
     *
     * @return bool|int
     */
    private function requestOperatorCount()
    {
        if( ($html = $this->makeRequest()) !== false ) {
            if( ($query_reset = $this->queryReset($html)) !== false ) {
                if( ($query_reset_html = $this->makeRequest($query_reset)) !== false ) {
                    if( ($operator_count = $this->parseOperatorCount($query_reset_html)) !== false ) {
                        return $operator_count;
                    }
                }
            }
        }

        return false;
    }

    /**
     *
     *
     * @return bool|int
     */
    private function requestOperatorLastID()
    {
        if( ($html = $this->makeRequest()) !== false ) {
            if( ($query_reset = $this->queryReset($html)) !== false ) {
                if( ($query_reset_html = $this->makeRequest($query_reset)) !== false ) {
                    if( ($operator_lastid = $this->parseOperatorLastID($query_reset_html)) !== false ) {
                        return $operator_lastid;
                    }
                }
            }
        }

        return false;
    }

    /**
     * @param $id
     *
     * @return array|bool
     */
    private function requestOperator($id)
    {
        $operator = array();

        if( ($html = $this->makeRequest($this->queryOperator($id))) !== false ) {
            if( ($info = $this->parseOperator($html)) !== false ) {

                if( ($accreditation = $this->requestOperatorAccreditation($html, $id))  !== false &&
                    ($service = $this->requestOperatorService($html, $id))              !== false &&
                    ($certification = $this->requestOperatorCertification($html, $id))  !== false &&
                    ($inspection = $this->requestOperatorInspection($html, $id))        !== false ){
                        $operator = array(
                            'operator'      => $info,
                            'accreditation' => $accreditation,
                            'service'       => $service,
                            'certification' => $certification,
                            'inspection'    => $inspection
                        );

                        return $operator;
                }

            } else {
                $this->error("Error: unable to parse main section for operator #$id");
            }
        }

        return false;
    }

    /**
     *
     *
     * @param $operator_html
     * @param $id
     *
     * @return array|bool
     */
    private function requestOperatorAccreditation($operator_html, $id)
    {
        // Информация об аккредитациях
        if( ($operator_accreditation_html = $this->makeRequest($this->queryOperatorAccreditation($operator_html, $id))) !== false ) {
            if( ($operator_accreditation_data = $this->parseOperatorAccreditation($operator_accreditation_html, $id)) !== false ) {
                return $operator_accreditation_data;
            } else {
                $this->error("Error: unable to parse accreditation section for operator #$id");
            }
        }

        return false;
    }

    /**
     *
     *
     * @param $operator_html
     * @param $id
     *
     * @return array|bool
     */
    private function requestOperatorService($operator_html, $id)
    {
        if( ($operator_service_html = $this->makeRequest($this->queryOperatorService($operator_html, $id))) !== false ) {
            if( ($operator_service_data = $this->parseOperatorService($operator_service_html, $id)) !== false ) {
                return $operator_service_data;
            } else {
                $this->error("Error: unable to parse service section for operator #$id");
            }
        }

        return false;
    }

    /**
     *
     *
     * @param $operator_html
     * @param $id
     *
     * @return array|bool
     */
    private function requestOperatorCertification($operator_html, $id)
    {
        if( ($html = $this->makeRequest($this->queryOperatorCertification($operator_html, $id))) !== false ) {
            if( ($data = $this->parseOperatorCertification($html, $id)) !== false ) {
                return $data;
            } else {
                $this->error("Error: unable to parse certification section for operator #$id");
            }
        }

        return false;
    }

    /**
     *
     *
     * @param $operator_html
     * @param $id
     *
     * @return array|bool
     */
    private function requestOperatorInspection($operator_html, $id)
    {
        if( ($html = $this->makeRequest($this->queryOperatorInspection($operator_html, $id))) !== false ) {
            if( ($data = $this->parseOperatorInspection($html, $id)) !== false ) {
                return $data;
            } else {
                $this->error("Error: unable to parse inspection section for operator #$id");
            }
        }

        return false;
    }

    /**
     *
     *
     * @param $query string
     *
     * @return bool|string
     */
    private function makeRequest($query=null)
    {
        try {
            if( is_array($query) && count($query) ) {
                $response = $this->http->post(self::RSA_URL, [
                    'body'          => http_build_query($query),
                    'headers'       => ['Content-Type' => 'application/x-www-form-urlencoded; charset=UTF-8'],
                    'debug'         => false,
                ]);
            } else {
                $response = $this->http->get(self::RSA_URL);
            }

            if( $response->getStatusCode() != 200 ) {
                $this->error("Error: request failed because status code is not equal 200!");
                return false;
            }

            return $response->getBody();

        } catch (\Exception $e) {
            $this->error("Error: ".$e->getMessage());
            return false;
        }

        return false;
    }

    /**
     *
     *
     * @return bool|string
     */
    private function getViewState()
    {
        $pattern = '/\<input.*?name=\"javax\.faces\.ViewState\".*?value=\"(.*?)\"/ms';
        if( ($html = $this->makeRequest()) !== false ) {
            $matches = array();
            if (preg_match($pattern, $html, $matches) == 1 && isset($matches[1]) ) {
                return $matches[1];
            }
        }

        $this->error('Error: Unable to parse View State!');
        return false;
    }

    /**
     *
     * @param $html
     *
     * @return bool|string
     */
    private function findResetField($html)
    {
        $pattern_form = '/\<div.*class=\"search-submit\"\>(.*?)\<\/div\>/ms';
        $pattern_inputs = '/\<input(.*?)\/\>/ms';
        $pattern_input = '/name=\"(.*?)\".*value=\"Сбросить поиск\"/ms';

        $matches = array();

        if( preg_match($pattern_form, $html, $matches) == 1 && isset($matches[1]) ) {
            $html = $matches[1];
            if( preg_match_all($pattern_inputs, $html, $matches) > 0 && isset($matches[1]) ) {
                $html = $matches[1];
                foreach( $html as $key => $value ) {
                    if( preg_match($pattern_input, $value, $matches) == 1 && isset($matches[1]) ) {
                        if( strlen($matches[1]) ) {
                            return $matches[1];
                        }
                    }
                }
            }
        }

        $this->error("Error: unable to find reset field!");
        return false;
    }

    /**
     *
     *
     * @param $html
     * @param $pattern_section
     *
     * @return bool|string
     */
    private function findOperatorSection($html, $pattern)
    {
        $matches = array();
        $pattern_links = '/\<div.*?class=\"filter-item\"\>(.*?)\<\/div\>/ms';
        if( preg_match_all($pattern_links, $html, $matches) > 0 && isset($matches[1])) {
            $html = $matches[1];
            foreach ( $html as $key => $value ) {
                if( preg_match($pattern, $value, $matches) == 1 && isset($matches[1]) && strlen($matches[1]) ) {
                    return $matches[1];
                }
            }
        }

        $this->error("Error: unable to find OTO Form link #ID!");
        return false;
    }

    /**
     *
     *
     * @param $html
     * @param $id
     * @param $pattern
     *
     * @return array|bool
     */
    private function createOperatorSectionQuery($html, $id, $pattern)
    {
        if( ($field_section = $this->findOperatorSection($html, $pattern)) !== false ) {
            return array(
                'javax.faces.partial.ajax'                => 'true',
                'javax.faces.source'                      => $field_section,
                'javax.faces.partial.execute'             => '@all',
                'javax.faces.partial.render'              => 'mainForm:contentTabsPanel mainForm:otoPanel',
                'mainForm:remoteCommandSetupOtoId_action' => 'mainForm:remoteCommandSetupOtoId_action',
                $field_section                            => $field_section,
                'otoId'                                   => $id,
                'mainForm'                                => 'mainForm',
                'mainForm:j_idt11'                        => $id,
                'mainForm:j_idt13'                        => '',
                'mainForm:j_idt14'                        => '',
                'mainForm:j_idt15'                        => '',
                'javax.faces.ViewState'                   => $this->getViewState()
            );
        }

        return false;
    }

    /**
     *
     *
     * @param $html
     *
     * @return array|bool
     */
    private function queryReset($html)
    {
        if( ($reset_field = $this->findResetField($html)) !== false ) {
            return array(
                'mainForm'              => 'mainForm',
                $reset_field            => 'Сбросить поиск',
                'javax.faces.ViewState' => $this->getViewState()
            );
        }

        return false;
    }

    /**
     *
     *
     * @param $id
     *
     * @return array
     */
    private function queryOperator($id)
    {
        return array(
            'javax.faces.partial.ajax'                => 'true',
            'javax.faces.source'                      => 'mainForm:remoteCommandSetupOtoId_action',
            'javax.faces.partial.execute'             => 'mainForm:remoteCommandSetupOtoId_action',
            'javax.faces.partial.render'              => 'mainForm:contentTabsPanel mainForm:otoPanel',
            'mainForm:remoteCommandSetupOtoId_action' => 'mainForm:remoteCommandSetupOtoId_action',
            'otoId'                                   => $id,
            'mainForm'                                => 'mainForm',
            'mainForm:j_idt10'                        => $id,
            'mainForm:j_idt12'                        => '',
            'mainForm:j_idt13'                        => '',
            'mainForm:j_idt15'                        => '',
            'javax.faces.ViewState'                   => $this->getViewState()
        );
    }

    /**
     *
     *
     * @param $html
     * @param $id
     *
     * @return array|bool
     */
    private function queryOperatorAccreditation($html, $id)
    {
        $pattern = '/\<a.*?id=\"(.*?)\".*?Информация об аккредитациях\<\/a\>/ms';
        return $this->createOperatorSectionQuery($html, $id, $pattern);
    }

    /**
     *
     *
     * @param $html
     * @param $id
     *
     * @return array|bool
     */
    private function queryOperatorService($html, $id)
    {
        $pattern = '/\<a.*?id=\"(.*?)\".*?Информация о пунктах ТО\<\/a\>/ms';
        return $this->createOperatorSectionQuery($html, $id, $pattern);
    }

    /**
     *
     *
     * @param $html
     * @param $id
     *
     * @return array|bool
     */
    private function queryOperatorCertification($html, $id)
    {
        $pattern = '/\<a.*?id=\"(.*?)\".*?Информация о приостановлении\/возобновлении\/аннулировании аттестата\<\/a\>/ms';
        return $this->createOperatorSectionQuery($html, $id, $pattern);
    }

    /**
     *
     *
     * @param $html
     * @param $id
     *
     * @return array|bool
     */
    private function queryOperatorInspection($html, $id)
    {
        $pattern = '/\<a.*?id=\"(.*?)\".*?Информация о проверках ОТО\<\/a\>/ms';
        return $this->createOperatorSectionQuery($html, $id, $pattern);
    }

    /**
     *
     *
     * @param $html
     *
     * @return int
     */
    private function parseOperatorLastID($html)
    {
        $matches = array();
        $pattern = '/\<table.*?class\=\"table\".*?\>.*?\<\/th\>.*?\<tbody\>.*?\<a.*?href\=\"#oto\=(\d+)\"\>/ms';
        if( preg_match($pattern, $html, $matches) == 1 ) {
            if( isset($matches[1]) && strlen($matches[1]) ) {
                $matches[1] = preg_replace("/[^0-9]/",'',$matches[1]);
                return intval($matches[1]);
            }
        }

        $this->error('Error: Unable to find last #ID of OTO');
        return 0;
    }

    /**
     *
     * @param $html
     *
     * @return int
     */
    private function parseOperatorCount($html)
    {
        $matches = array();
        $pattern = '/Всего.*?\<b\>(.*?)\<\/b\>.*?операторов/ms';
        if( preg_match($pattern, $html, $matches) == 1 ) {
            if( isset($matches[1]) && strlen($matches[1]) ) {
                $matches[1] = preg_replace("/[^0-9]/",'',$matches[1]);
                return intval($matches[1]);
            }
        }

        $this->error('Error: Unable to find OTO count!');
        return 0;
    }

    /**
     * @param $data
     *
     * @return int
     */
    private  function parseFieldStatus($data)
    {
        if( preg_match('/действителен/i', $data, $matches) ) {
            return 2;
        } else if( preg_match('/приостановлен/i', $data, $matches) ) {
            return 3;
        } else if( preg_match('/аннулирован/i', $data, $matches) ) {
            return 4;
        }

        return 1;
    }

    /**
     * @param $data
     * @return array
     */
    private function parseFieldCategories($data)
    {
        // TODO: Get data from database and compare
        $data = preg_replace('/[^0-9A-Z\,]/i', '', $data);
        $data = explode(',', $data);
        foreach ($data as $k => $v ) {
            switch($v){
                case 'L':  $data[$k] = 2;   break;
                case 'M1': $data[$k] = 3;   break;
                case 'M2': $data[$k] = 4;   break;
                case 'M3': $data[$k] = 5;   break;
                case 'N1': $data[$k] = 6;   break;
                case 'N2': $data[$k] = 7;   break;
                case 'N3': $data[$k] = 8;   break;
                case 'O':  $data[$k] = 9;   break;
                case 'O1': $data[$k] = 10;   break;
                case 'O2': $data[$k] = 11;  break;
                case 'O3': $data[$k] = 12;  break;
                case 'O4': $data[$k] = 13;  break;
                case 'TB': $data[$k] = 14;  break;
                case 'TM': $data[$k] = 15;  break;
                default:   $data[$k] = 1;
            }
        }

        return $data;
    }

    /**
     * @param $html
     *
     * @return array|bool
     */
    private function parseOperator($html)
    {
        //echo $html;
        $patterns = array(
            'rsa_id'    => '/red-part\"\>(\d+)\<\/div\>/ms',                                        // НОМЕР ОТО
            'status_id' => '/status-block.*?\>.*?Статус\<\/div\>.*\<p\>Аттестат.(.*?)\<\/p\>/ms',   // СТАТУС
            'shortname' => '/red-part\"\>\d+\<\/div\>.*?\<div.*?title\"\>(.*?)\<\/div\>/ms',        // КРАТКОЕ НАИМЕНОВАНИЕ
            'fullname'  => '/Полное наименование\<\/div\>.*?\<p\>(.*?)\<\/p\>/ms',                  // ПОЛНОЕ НАИМЕНОВАНИЕ
            'manager'   => '/Директор\<\/div\>.*?\<p\>(.*?)\<\/p\>/ms',                             // ДИРЕКТОР
            'ogrn'      => '/ОГРН\/ОГРНИП\<\/div\>.*?\<p\>(\d+)\<\/p\>/ms',                         // ОГРН/ОГРНИП
            'address'   => '/Адрес\<\/div\>.*?\<p\>(.*?)\<\/p\>/ms',                                // АДРЕС
            'phone'     => '/Телефон\<\/div\>.*?\<p\>(.*?)\<\/p\>/ms',                              // ТЕЛЕФОН
        );

        $data = array();
        $matches = array();

        foreach ( $patterns as $key => $pattern ) {
            $data[$key] = null;
            if( preg_match($pattern, $html, $matches) == 1 && isset($matches[1])) {
                $data[$key] = strip_tags($matches[1]);
                switch ( $key ) {
                    case 'rsa_id':
                    case 'ogrn':
                        $data[$key] = intval($matches[1]);
                        break;

                    case 'status_id':
                        $data[$key] = $this->parseFieldStatus($matches[1]);
                        break;
                }
            } else {
                return false;
            }
        }

        return $data;
    }

    /**
     * @param $html
     * @param $id
     * @return array|bool
     */
    private function parseOperatorAccreditation($html, $id)
    {

        $table_body = '/\<table.*?\>.*?\<thead.*?\>.*?\<th.*?\>Вид решения\<\/th\>.*?Кол-во Пто\<\/th\>.*?\<tbody\>(.*?)\<\/tbody\>/ms';
        $table_rows = '/\<tr\>.*?\<\/tr\>/ms';

        $patterns = array(
            'type'       => '/\<td\>(.*?)\<\/td\>.*?\<td\>.*?\<\/td>.*?\<td\>.*?\<\/td>.*?\<td\>.*?\<\/td>.*?\<td\>.*?\<\/td>.*?\<td\>.*?\<\/td>/ms', // ВИД РЕШЕНИЯ
            'certnum'    => '/\<td\>.*?\<\/td\>.*?\<td\>(.*?)\<\/td>.*?\<td\>.*?\<\/td>.*?\<td\>.*?\<\/td>.*?\<td\>.*?\<\/td>.*?\<td\>.*?\<\/td>/ms', // НОМЕР АТТЕСТАТА
            'orderdate'  => '/\<td\>.*?\<\/td\>.*?\<td\>.*?\<\/td>.*?\<td\>(.*?)\<\/td>.*?\<td\>.*?\<\/td>.*?\<td\>.*?\<\/td>.*?\<td\>.*?\<\/td>/ms', // ДАТА ПРИКАЗА
            'ordernum'   => '/\<td\>.*?\<\/td\>.*?\<td\>.*?\<\/td>.*?\<td\>.*?\<\/td>.*?\<td\>(.*?)\<\/td>.*?\<td\>.*?\<\/td>.*?\<td\>.*?\<\/td>/ms', // НОМЕР ПРИКАЗА
            'categories' => '/\<td\>.*?\<\/td\>.*?\<td\>.*?\<\/td>.*?\<td\>.*?\<\/td>.*?\<td\>.*?\<\/td>.*?\<td\>(.*?)\<\/td>.*?\<td\>.*?\<\/td>/ms', // ОБЛАСТЬ АККРЕДИТАЦИИ
            'services'   => '/\<td\>.*?\<\/td\>.*?\<td\>.*?\<\/td>.*?\<td\>.*?\<\/td>.*?\<td\>.*?\<\/td>.*?\<td\>.*?\<\/td>.*?\<td\>(.*?)\<\/td>/ms', // КОЛ-ВО ПТО
        );

        $data = array();
        $matches = array();

        if( preg_match($table_body, $html, $matches) == 1 && isset($matches[1]) ) {
            $html = $matches[1];
            if( preg_match_all($table_rows, $html, $matches) > 0 && isset($matches[0]) ) {
                $html = $matches[0];
                foreach ( $html as $row_key => $row_value ) {
                    foreach ( $patterns as $key => $pattern ) {
                        $data[$row_key][$key] = null;
                        if( preg_match($pattern, $row_value, $matches) == 1 && isset($matches[1]) ) {
                            $data[$row_key][$key] = strip_tags($matches[1]);

                            switch( $key ) {
                                case 'certnum':
                                case 'services':
                                    $data[$row_key][$key] = intval($data[$row_key][$key]);
                                    break;
                                case 'orderdate':
                                    $data[$row_key][$key] = date("Y.m.d", strtotime($data[$row_key][$key]));
                                    break;
                                case 'categories':
                                    $data[$row_key][$key] = $this->parseFieldCategories($data[$row_key][$key]);
                                    break;
                            }

                        } else {
                            return false;
                        }
                    }
                    $data[$row_key]['operator_id'] = $id;
                }
            }
        }

        return $data;
    }

    /**
     * @param $html
     * @param $id
     * @return array|bool
     */
    private function parseOperatorService($html, $id)
    {
        $table_body = '/\<table.*?\>.*?\<thead.*?\>.*?\<th.*?\>Статус\<\/th\>.*?Область аккредитации\<\/th\>.*?\<tbody\>(.*?)\<\/tbody\>/ms';
        $table_rows = '/\<tr\>.*?\<\/tr\>/ms';

        $patterns = array(
            'rsa_id'     => '/\<td\>\<a.*?href=\"#pto\=(\d+)\"\>/ms',                                           // ИДЕНТИФИКАТОР ПТО
            'address'    => '/\<td\>.*?\<\/td\>.*?\<td\>(.*?)\<\/td>.*?\<td\>.*?\<\/td>.*?\<td\>.*?\<\/td>/ms', // АДРЕС ПТО
            'phone'      => '/\<td\>.*?\<\/td\>.*?\<td\>.*?\<\/td>.*?\<td\>(.*?)\<\/td>.*?\<td\>.*?\<\/td>/ms', // ТЕЛЕФОН
            'categories' => '/\<td\>.*?\<\/td\>.*?\<td\>.*?\<\/td>.*?\<td\>.*?\<\/td>.*?\<td\>(.*?)\<\/td>/ms', // ОБЛАСТЬ АККРЕДИТАЦИИ
        );

        $data = array();
        $matches = array();

        if( preg_match($table_body, $html, $matches) == 1 && isset($matches[1]) ) {
            $html = $matches[1];
            if( preg_match_all($table_rows, $html, $matches) > 0 && isset($matches[0]) ) {
                $html = $matches[0];
                foreach ( $html as $row_key => $row_value ) {
                    foreach ( $patterns as $key => $pattern ) {
                        $data[$row_key][$key] = null;
                        if( preg_match($pattern, $row_value, $matches) == 1 && isset($matches[1]) ) {
                            $data[$row_key][$key] = strip_tags($matches[1]);

                            switch($key){
                                case 'rsa_id':
                                    if( $key == 'rsa_id' ) $data[$row_key][$key] = intval($data[$row_key][$key]);
                                    break;
                                case 'categories':
                                    $data[$row_key][$key] = $this->parseFieldCategories($data[$row_key][$key]);
                                    break;
                            }

                        } else {
                            return false;
                        }
                        $data[$row_key]['operator_id'] = $id;
                    }
                }
            }
        }

        return $data;
    }

    /**
     * @param $html
     * @param $id
     * @return array|bool
     */
    private function parseOperatorCertification($html, $id)
    {
        $table_body = '/\<table.*?\>.*?\<thead.*?\>.*?\<th.*?\>Основание вынесения решения\<\/th\>.*?№ приказа\<\/th\>.*?\<tbody\>(.*?)\<\/tbody\>/ms';
        $table_rows = '/\<tr\>.*?\<\/tr\>/ms';

        $patterns = array(
            'subject'   => '/\<td\>(.*?)\<\/td\>.*?\<td\>.*?\<\/td>.*?\<td\>.*?\<\/td>.*?\<td\>.*?\<\/td>/ms', // ОСНОВАНИЕ ВЫНЕСЕНИЯ РЕШЕНИЯ
            'type'      => '/\<td\>.*?\<\/td\>.*?\<td\>(.*?)\<\/td>.*?\<td\>.*?\<\/td>.*?\<td\>.*?\<\/td>/ms', // ТИП РЕШЕНИЯ
            'orderdate' => '/\<td\>.*?\<\/td\>.*?\<td\>.*?\<\/td>.*?\<td\>(.*?)\<\/td>.*?\<td\>.*?\<\/td>/ms', // ДАТА ПРИКАЗА
            'ordernum'  => '/\<td\>.*?\<\/td\>.*?\<td\>.*?\<\/td>.*?\<td\>.*?\<\/td>.*?\<td\>(.*?)\<\/td>/ms', // № ПРИКАЗА
        );

        $data = array();
        $matches = array();

        if( preg_match($table_body, $html, $matches) == 1 && isset($matches[1]) ) {
            $html = $matches[1];
            if( preg_match_all($table_rows, $html, $matches) > 0 && isset($matches[0]) ) {
                $html = $matches[0];
                foreach ( $html as $row_key => $row_value ) {
                    foreach ( $patterns as $key => $pattern ) {
                        $data[$row_key][$key] = null;
                        if( preg_match($pattern, $row_value, $matches) == 1 && isset($matches[1]) ) {
                            $data[$row_key][$key] = strip_tags($matches[1]);
                            switch ( $key ) {
                                case 'orderdate':
                                    $data[$row_key][$key] = date("Y.m.d", strtotime($data[$row_key][$key]));
                                    break;
                            }
                        } else {
                            return false;
                        }
                        $data[$row_key]['operator_id'] = $id;
                    }
                }
            }
        }

        return $data;
    }

    /**
     * @param $html
     * @param $id
     * @return array|bool
     */
    private function parseOperatorInspection($html, $id)
    {
        $table_body = '/\<table.*?\>.*?\<thead.*?\>.*?\<th.*?\>Основание для проведения проверки\<\/th\>.*?Результат проверки\<\/th\>.*?\<tbody\>(.*?)\<\/tbody\>/ms';
        $table_rows = '/\<tr\>.*?\<\/tr\>/ms';

        $patterns = array(
            'number'          => '/\<td\>(.*?)\<\/td\>.*?\<td\>.*?\<\/td>.*?\<td\>.*?\<\/td>.*?\<td\>.*?\<\/td>.*?\<td\>.*?\<\/td>.*?\<td\>.*?\<\/td>.*?\<td\>.*?\<\/td>.*?\<td\>.*?\<\/td>/ms', // №
            'subject'         => '/\<td\>.*?\<\/td\>.*?\<td\>(.*?)\<\/td>.*?\<td\>.*?\<\/td>.*?\<td\>.*?\<\/td>.*?\<td\>.*?\<\/td>.*?\<td\>.*?\<\/td>.*?\<td\>.*?\<\/td>.*?\<td\>.*?\<\/td>/ms', // ОСНОВАНИЕ ДЛЯ ПРОВЕДЕНИЯ ПРОВЕРКИ
            'checkdate'       => '/\<td\>.*?\<\/td\>.*?\<td\>.*?\<\/td>.*?\<td\>(.*?)\<\/td>.*?\<td\>.*?\<\/td>.*?\<td\>.*?\<\/td>.*?\<td\>.*?\<\/td>.*?\<td\>.*?\<\/td>.*?\<td\>.*?\<\/td>/ms', // ДАТА ПРОВЕДЕНИЯ ПРОВЕРКИ
            'actdate'         => '/\<td\>.*?\<\/td\>.*?\<td\>.*?\<\/td>.*?\<td\>.*?\<\/td>.*?\<td\>(.*?)\<\/td>.*?\<td\>.*?\<\/td>.*?\<td\>.*?\<\/td>.*?\<td\>.*?\<\/td>.*?\<td\>.*?\<\/td>/ms', // ДАТА АКТА/ЗАКЛЮЧЕНИЯ
            'service_address' => '/\<td\>.*?\<\/td\>.*?\<td\>.*?\<\/td>.*?\<td\>.*?\<\/td>.*?\<td\>.*?\<\/td>.*?\<td\>(.*?)\<\/td>.*?\<td\>.*?\<\/td>.*?\<td\>.*?\<\/td>.*?\<td\>.*?\<\/td>/ms', // АДРЕС ПТО
            'violations'      => '/\<td\>.*?\<\/td\>.*?\<td\>.*?\<\/td>.*?\<td\>.*?\<\/td>.*?\<td\>.*?\<\/td>.*?\<td\>.*?\<\/td>.*?\<td\>(.*?)\<\/td>.*?\<td\>.*?\<\/td>.*?\<td\>.*?\<\/td>/ms', // ВЫЯВЛЕННЫЕ НАРУШЕНИЯ
            'document'        => '/\<td\>.*?\<\/td\>.*?\<td\>.*?\<\/td>.*?\<td\>.*?\<\/td>.*?\<td\>.*?\<\/td>.*?\<td\>.*?\<\/td>.*?\<td\>.*?\<\/td>.*?\<td\>(.*?)\<\/td>.*?\<td\>.*?\<\/td>/ms', // РЕКВИЗИТЫ ПРИНЯТОГО РЕШЕНИЯ
            'result'          => '/\<td\>.*?\<\/td\>.*?\<td\>.*?\<\/td>.*?\<td\>.*?\<\/td>.*?\<td\>.*?\<\/td>.*?\<td\>.*?\<\/td>.*?\<td\>.*?\<\/td>.*?\<td\>.*?\<\/td>.*?\<td\>(.*?)\<\/td>/ms', // РЕЗУЛЬТАТ ПРОВЕРКИ
        );

        $data = array();
        $matches = array();

        if( preg_match($table_body, $html, $matches) == 1 && isset($matches[1]) ) {
            $html = $matches[1];
            if( preg_match_all($table_rows, $html, $matches) > 0 && isset($matches[0]) ) {
                $html = $matches[0];
                foreach ( $html as $row_key => $row_value ) {
                    foreach ( $patterns as $key => $pattern ) {
                        $data[$row_key][$key] = null;
                        if( preg_match($pattern, $row_value, $matches) == 1 && isset($matches[1]) ) {
                            $data[$row_key][$key] = strip_tags($matches[1]);
                            if( $key == 'number' ) $data[$row_key][$key] = intval($matches[1]);
                            switch ( $key ) {
                                case 'actdate':
                                    $data[$row_key][$key] = date("Y.m.d", strtotime($data[$row_key][$key]));
                                    break;
                            }
                        } else {
                            return false;
                        }
                        $data[$row_key]['operator_id'] = $id;
                    }
                }
            }
        }

        return $data;
    }
}
