<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Accreditation extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $dateFormat = 'Y.m.d';

    /**
     * @var array
     */
    protected $dates = [
        'orderdate',
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'operator_id',
        'type'       ,
        'certnum'    ,
        'orderdate'  ,
        'ordernum'   ,
        'services'   ,
    ];

    public function categories()
    {
        return $this->belongsToMany(
            'App\Category',
            'category_accreditations',
            'accreditation_id',
            'category_id'

        );
    }
}
