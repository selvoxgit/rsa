<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Operator extends Model
{
    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var string
     */
    protected $dateFormat = 'Y.m.d H:i:s';

    /**
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    /**
     * @var array
     */
    protected $fillable = [
        'rsa_id'    ,
        'status_id' ,
        'shortname' ,
        'fullname'  ,
        'manager'   ,
        'ogrn'      ,
        'address'   ,
        'phone'     ,
        'created_at',
        'updated_at',
    ];

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status()
    {
        return $this->belongsTo('App\Status');
    }

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function accreditations()
    {
        return $this->hasMany('App\Accreditation');
    }

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function services()
    {
        return $this->hasMany('App\Service');
    }

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function certifications()
    {
        return $this->hasMany('App\Certification');
    }

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function inspections()
    {
        return $this->hasMany('App\Inspection');
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return int
     */
    public function scopeOperatorCount($query)
    {
        return $query->count('id');
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return mixed
     */
    public function scopeLastUpdated($query)
    {
        return $query->select('created_at')->orderByDesc('created_at')->get();
    }
}
