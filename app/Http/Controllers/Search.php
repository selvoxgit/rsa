<?php

namespace App\Http\Controllers;

use App\Accreditation;
use App\Category;
use App\CategoryAccreditations;
use App\CategoryServices;
use App\Certification;
use App\Inspection;
use App\Operator;
use App\Service;
use App\Status;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use mysql_xdevapi\Session;

class Search extends Controller
{
    public function __construct()
    {
        // admin@selvox.ru - 3w2sbMSBKTDBhie
        //$this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Load accreditation categories
        $categories = Category::all();

        // Load status of company
        $statuses = Status::all();

        // Load all companies
        $operators = Operator::paginate(env('OTO_ITEMS_PER_PAGE', 30));

        // Show main page
        return view('search.index')
            ->with('operators', $operators)
            ->with('categories', $categories)
            ->with('statuses', $statuses);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }



    public function operator_info(Request $request, $id)
    {
        if( $request->isMethod('post') ) {
            $id = $request->input('id', 0);
        }
        $query = Operator::query();
        if( intval($id) > 0 ) {
            $query->where('id', '=', intval($id));
            $operator = $query->first();

            $data = view('/search/templates/result/operators/operator')
                ->with('operator', $operator)
                ->with('id', $id)
                ->render();
            return response()->json(array(
                'msg' => $data
            ), 200, ['Content-Type' => 'application/json']);
        }

        abort(404);
    }

    /**
     * Поиск по Операторам ТО
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search_operators(Request $request)
    {
        $categories = Category::all();
        $statuses = Status::all();
        $query = Operator::query();

        // Поиск по идентификатору
        if( $request->input('id') !== null && intval($request->input('id')) > 0 ) {
            $query->where('rsa_id', '=', intval($request->input('id')));
        }

        // Поиск по Директору
        if( $request->input('manager') !== null && strlen($request->input('manager')) ) {
            $query->where('manager', 'like', '%'.$request->input('manager').'%');
        }

        // Поиск по названию ОТО
        if( $request->input('company') !== null && strlen($request->input('company')) ) {
            $query->where(function($query){
                global $request;
                $query->where('shortname', 'like', '%'.$request->input('company').'%')
                      ->orWhere('fullname', 'like', '%'.$request->input('company').'%');
            });
        }

        // Поиск по ОГРН
        if( $request->input('ogrn') !== null && intval($request->input('ogrn')) > 0 ) {
            if( strlen($request->input('ogrn')) == 13 ) {
                $query->where('ogrn', '=', intval($request->input('ogrn')));
            }
        }

        // Поиск по адресу
        if( $request->input('address') !== null && strlen($request->input('address')) ) {
            $query->where('address', 'like', '%'.$request->input('address').'%');
        }

        // Поиск по статусу
        if( $request->input('status') !== null && intval($request->input('status')) > 1 ) {
            $query->where('status_id', '=', intval($request->input('status')));
        }

        //var_dump($request->all());
        if( $request->input('service_count_expression') !== null && strlen($request->input('service_count_expression')) == 1 &&
            $request->input('service_count_value') !== null && intval($request->input('service_count_value')) > 0  ) {
            $query->withCount('services');
        }

        echo vsprintf(str_replace(['?'], ['\'%s\''], $query->toSql()), $query->getBindings());
        $operators = $query->paginate(env('OTO_ITEMS_PER_PAGE', 15));
        return view('search/index')
                    ->with('operators', $operators)
                    ->with('categories', $categories)
                    ->with('statuses', $statuses)
                    ->with('form', $request->all());
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search_services(Request $request)
    {
        return view('search/index');
    }

}
