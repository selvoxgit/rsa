<?php

namespace App\Http\Controllers;

use App\Category;
use App\Operator;
use App\Status;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Helper\Table;

class Operators extends Controller
{
    /**
     * Конструктор класса.
     *
     * @return void
     */
    public function __construct()
    {
        if( Config::get('rsa.auth_enable', false) === true ) {
            $this->middleware('auth');
        }
    }

    /**
     * Функция отображает список операторов в соответствии с поисковым запросом.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        // Список категорий
        $categories = Category::all();

        // Список статусов
        $statuses = Status::all();

        // Запрос БД
        $query = Operator::query();

        // Если передена форма запроса, то
        if( $request->isMethod('POST') ) {

            // Сохраняем поисковый запрос в сессии
            $request->session()->put('operators', $request->all());

            // Храним данные только для следующего запроса
            $request->flash();

        // Если пользователь зашел по url оператора, то
        } elseif ( $request->isMethod('GET') ) {

            // Если форма запроса была сохранена в сессии, то
            if( $request->session()->has('operators') && intval($request->get('page',0)) > 0 ) {

                // Получаем данные запроса из сессии
                $data = $request->session()->get('operators', null);

                // Возвращаем данные запроса назад в request
                if( is_array($data) && count($data) ) $request->request->add($data);
            }
        }

        // Поиск по полю: id
        if( $request->input('id') !== null && intval($request->input('id')) > 0 ) {
            $query->where('rsa_id', '=', intval($request->input('id')));
        }

        // Поиск по полю: manager
        if( $request->input('manager') !== null && strlen($request->input('manager')) ) {
            $query->where('manager', 'like', '%'.$request->input('manager').'%');
        }

        // Поиск по полю: company
        if( $request->input('company') !== null && strlen($request->input('company')) ) {
            $query->where(function($query){
                global $request;
                $query->where('shortname', 'like', '%'.$request->input('company').'%')
                    ->orWhere('fullname', 'like', '%'.$request->input('company').'%');
            });
        }

        // Поиск по полю: psrn
        if( $request->input('psrn') !== null && intval($request->input('psrn')) > 0 ) {
            if( strlen($request->input('psrn')) == 13 ) {
                $query->where('ogrn', '=', intval($request->input('psrn')));
            }
        }

        // Поиск по полю: address
        if( $request->input('address') !== null && strlen($request->input('address')) ) {
            $query->where('address', 'like', '%'.$request->input('address').'%');
        }

        // Поиск по полю: status
        if( $request->input('status') !== null && intval($request->input('status')) > 1 ) {
            if( $request->input('status_date_from') !== null      &&
                strlen($request->input('status_date_from')) == 10 ||
                $request->input('status_date_to') !== null        &&
                strlen($request->input('status_date_to')) == 10   ){

                // Поиск аннулированных сертификатор по датам
                if( intval($request->input('status')) == 4 ) {
                    $query->whereIn('id', function (Builder $query) {
                        $query->select('operator_id')->from('accreditations')
                        ->where('type', 'like', '%Аннулирование%');

                        if( request()->input('status_date_from') !== null      &&
                            strlen(request()->input('status_date_from')) == 10 &&
                            request()->input('status_date_to') !== null        &&
                            strlen(request()->input('status_date_to')) == 10   ){
                                $query->whereBetween(DB::raw('date(orderdate)'), [
                                    request()->input('status_date_from'),
                                    request()->input('status_date_to')
                                ], 'AND');

                        } else if( request()->input('status_date_from') !== null      &&
                                   strlen(request()->input('status_date_from')) == 10 ){
                            $query->where(DB::raw('date(orderdate)'), '=',
                                request()->input('status_date_from'));

                        } else  if( request()->input('status_date_to') !== null &&
                                    strlen(request()->input('status_date_to')) == 10 ) {
                            $query->where(DB::raw('date(orderdate)'), '=',
                                request()->input('status_date_to'));
                        }
                    });

                // Поиск приостановленных сертификатов по датам
                } else if( intval($request->input('status')) == 3 ) {
                    $query->whereIn('id', function (Builder $query) {
                        $query->select('operator_id')->from('certifications')
                            ->where('type', 'like', '%Приостановление%');

                        if( request()->input('status_date_from') !== null && strlen(request()->input('status_date_from')) == 10 &&
                            request()->input('status_date_to') !== null && strlen(request()->input('status_date_to')) == 10 ) {
                            $query->whereBetween(DB::raw('date(orderdate)'), [
                                request()->input('status_date_from'),
                                request()->input('status_date_to')
                            ], 'AND');

                        } else if( request()->input('status_date_from') !== null && strlen(request()->input('status_date_from')) == 10 ) {
                            $query->where(DB::raw('date(orderdate)'), '=', request()->input('status_date_from'));

                        } else  if( request()->input('status_date_to') !== null && strlen(request()->input('status_date_to')) == 10 ) {
                            $query->where(DB::raw('date(orderdate)'), '=', request()->input('status_date_to'));
                        }
                    });

                // Поиск действуюших компаний
                } else if( intval($request->input('status')) == 2 ) {

                    $query->whereIn('id', function (Builder $query) {
                        $query->select('operator_id')->from('accreditations')
                            ->where('type', 'like', '%Первичная аккредитация%');

                        if( request()->input('status_date_from') !== null      &&
                            strlen(request()->input('status_date_from')) == 10 &&
                            request()->input('status_date_to') !== null        &&
                            strlen(request()->input('status_date_to')) == 10   ){
                            $query->whereBetween(DB::raw('date(orderdate)'), [
                                request()->input('status_date_from'),
                                request()->input('status_date_to')
                            ], 'AND');

                        } else if( request()->input('status_date_from') !== null      &&
                            strlen(request()->input('status_date_from')) == 10 ){
                            $query->where(DB::raw('date(orderdate)'), '=',
                                request()->input('status_date_from'));

                        } else  if( request()->input('status_date_to') !== null &&
                            strlen(request()->input('status_date_to')) == 10 ) {
                            $query->where(DB::raw('date(orderdate)'), '=',
                                request()->input('status_date_to'));
                        }
                    });
                }

                /** Аннулированные аттестаты
                select * from `operators` where operators.id IN(
                    SELECT operator_id FROM accreditations
                    WHERE `certnum`=0
                    AND `type` like '%Аннулирование%'
                    AND (orderdate >= '2019-10-20' AND orderdate <= '2019-10-30')
                )**/
            } else {
                $query->where('status_id', '=', intval($request->input('status')));
            }
        }

        //echo vsprintf(str_replace(['?'], ['\'%s\''], $query->toSql()), $query->getBindings());
        //die();

        // Поиск по полю: categories
        if( $request->input('categories') !== null && is_array($request->input('categories')) ) {
            $query->whereIn('id', function (Builder $query) {
                global $request;
                $orWhere = false;
                $query->select('operator_id')
                    ->from('category_services')
                    ->join('categories', 'categories.id', '=', 'category_services.category_id')
                    ->join('services', 'services.id', '=', 'category_services.service_id');
                    foreach( $request->input('categories') as $category_key => $category_val ) {
                        if( $orWhere == false ) {
                            $query->where('categories.id', '=', intval($category_val));
                            $orWhere = true;
                        } else {
                            $query->orWhere('categories.id', '=', intval($category_val));
                        }
                    }
            });
        }

        // Поиск по полям: service_count_expression, service_count_value
        if( $request->input('service_count_expression') !== null     &&
            strlen($request->input('service_count_expression')) == 1 &&
            $request->input('service_count_value') !== null          &&
            intval($request->input('service_count_value')) > 0       ){
            if( $request->input('service_count_expression') == 'g' ||
                $request->input('service_count_expression') == 'l' ||
                $request->input('service_count_expression') == 'e' ){
                $query->whereIn('id', function (Builder $query) {
                    global $request;
                    $expression = '=';
                    if( mb_strtolower($request->input('service_count_expression')) == 'g' ) $expression = '>';
                    if( mb_strtolower($request->input('service_count_expression')) == 'l' ) $expression = '<';
                    if( mb_strtolower($request->input('service_count_expression')) == 'e' ) $expression = '=';
                    $query->select('operator_id')
                          ->from('services')
                          ->groupBy('operator_id')
                          ->having(DB::raw('COUNT(id)'),$expression, intval($request->input('service_count_value')));
                });
            }
        }

        // Общее кол-во операторов
        $items_total = Operator::operatorCount();
        $lastrecord = DB::table('operators')->latest()->first();
        //$parsed = $lastrecord->created_at();

        // Найдено операторов
        $items_total_query = clone $query;
        $items_found = $items_total_query->get()->count();

        $operators = $query->paginate(env('APP_ITEMS_PER_PAGE', 30));

        // Вывод запроса на экран
        //echo vsprintf(str_replace(['?'], ['\'%s\''], $query->toSql()), $query->getBindings());

        // Вывод результатов поиска
        return view('operators.search.screen')
            ->with('operators', $operators)
            ->with('categories', $categories)
            ->with('statuses', $statuses)
            ->with('form', $request->all())
            ->with('items_total', $items_total)
            ->with('items_found', $items_found)
            ->with('parsed', $lastrecord);
    }

    /**
     * Функция выводит информацию о выбранном операторе.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @throws \Throwable
     */
    public function show(Request $request, $id)
    {
        // Запрос БД
        $query = Operator::query();

        // Если передена форма запроса, то
        if( $request->isMethod('POST') ) {

            // Если передан идентифиатор оператора, то
            if ( $request->input('id') !== null && intval($request->input('id')) > 0 ) {

                // Составляем запрос в БД по идентификатору оператора
                $query->where('id', '=', intval($request->input('id')));

                // Получае первую найденную запись
                $operator = $query->first();

                // Вормируем json-ответ вэб сервера
                return response()->json(array(
                    'msg' => view('operators.show._operator')->with('operator', $operator)->render()
                ), 200, ['Content-Type' => 'application/json']);
            }
        // Если пользователь зашел по url оператора, то
        } else if( $request->isMethod('GET') ) {

            // Если передан идентифиатор оператора, то
            if ( isset($id)  && $id !== null && intval($id) > 0) {

                // Составляем запрос в БД по идентификатору оператора
                $query->where('id', '=', intval($id));

                // Получае первую найденную запись
                $operator = $query->first();

                // Вормируем ответ вэб сервера
                return view('operators.show.screen')->with('operator', $operator);
            }
        }

        abort(404);
    }
}
