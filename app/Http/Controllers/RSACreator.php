<?php

namespace App\Http\Controllers;

use App\Accreditation;
use App\CategoryAccreditations;
use App\CategoryServices;
use App\Certification;
use App\Inspection;
use App\Operator;
use App\Service;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;

class RSACreator extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  array
     * @return void
     */
    public function store($data)
    {
        $validator = Validator::make($data['operator'], [
            'rsa_id'        => 'required|integer',
            'status'        => 'required|integer',
            'shortname'     => 'max:255',
            'fullname'      => 'max:255',
            'manager'       => 'required|max:100',
            'ogrn'          => 'integer',
            'address'       => 'max:255',
            'phone'         => 'max:255',
            'email'         => 'max:255',
        ]);

        $data['operator']['updated_at'] = Carbon::createFromFormat("Y-m-d H:s:i", $this->makeUpdatedAt());

        $operator_model = new Operator();
        $operator_model->fill($data['operator']);
        $operator_model->save();

        foreach ( $data['accreditation'] as $accreditation_id => $accreditation_data) {
            $accreditation_model = new Accreditation($accreditation_data);
            $operator_model->accreditations()->save($accreditation_model);
            foreach ($accreditation_data['categories'] as $category_key => $category_value ){
                $category_accreditation_model = new CategoryAccreditations();
                $category_accreditation_model->category_id = $category_value;
                $category_accreditation_model->accreditation_id = $accreditation_model->id;
                $category_accreditation_model->save();
            }
        }

        foreach ( $data['service'] as $service_id => $service_data ) {
            $service_model = new Service($service_data);
            $operator_model->services()->save($service_model);
            foreach ($service_data['categories'] as $category_key => $category_value ){
                $category_service_model = new CategoryServices();
                $category_service_model->category_id = $category_value;
                $category_service_model->service_id = $service_model->id;
                $category_service_model->save();
            }
        }

        foreach ( $data['certification'] as $certification_id => $certification_data ) {
            $certification_model = new Certification($certification_data);
            $operator_model->certifications()->save($certification_model);
        }

        foreach ( $data['inspection'] as $inspection_id => $inspection_data ) {
            $inspection_model = new Inspection($inspection_data);
            $operator_model->inspections()->save($inspection_model);
        }
    }

    protected function makeUpdatedAt()
    {
        return date("Y-m-d H:i:s", strtotime( sprintf("%s-%s-%s %s:%s:%s",
            date("Y", time()),
            date("m", time()),
            date("d", time()),
            str_pad(mt_rand(0,23), 2, "0", STR_PAD_LEFT),
            str_pad(mt_rand(0,59), 2, "0", STR_PAD_LEFT),
            str_pad(mt_rand(0,59), 2, "0", STR_PAD_LEFT))
        ));
    }
}
