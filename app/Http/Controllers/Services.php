<?php

namespace App\Http\Controllers;

use App\Category;
use App\Operator;
use App\Service;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class Services extends Controller
{
    /**
     * Конструктор класса.
     *
     * @return void
     */
    public function __construct()
    {
        if( Config::get('rsa.auth_enable', false) === true ) {
            $this->middleware('auth');
        }
    }

    /**
     * Функция отображает список операторов в соответствии с поисковым запросом.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        // Список категорий
        $categories = Category::all();

        // Запрос БД
        $query = Service::query();

        // Если передена форма запроса, то
        if( $request->isMethod('POST') ) {

            // Сохраняем поисковый запрос в сессии
            $request->session()->put('services', $request->all());

            // Храним данные только для следующего запроса
            $request->flash();

            // Если пользователь зашел по url оператора, то
        } elseif ( $request->isMethod('GET') ) {

            // Если форма запроса была сохранена в сессии, то
            if( $request->session()->has('services') && intval($request->get('page', 0)) > 0 ) {

                // Получаем данные запроса из сессии
                $data = $request->session()->get('services', null);

                // Возвращаем данные запроса назад в request
                if( is_array($data) && count($data) ) $request->request->add($data);
            }
        }

        // Поиск по полю: id
        if( $request->input('id') !== null && intval($request->input('id')) > 0 ) {
            $query->where('services.rsa_id', '=', intval($request->input('id')));
        }

        // Поиск по полю: categories
        if( $request->input('categories') !== null && is_array($request->input('categories')) ) {
            $query->join('operators', 'services.operator_id', '=', 'operators.id');
            $query->whereIn('services.id', function (Builder $query) {
                global $request;
                $orWhere = false;
                $query->select('category_services.service_id')
                    ->from('category_services')
                    ->join('categories', 'categories.id', '=', 'category_services.category_id')
                    ->join('services', 'services.id', '=', 'category_services.service_id');
                foreach ($request->input('categories') as $category_key => $category_val) {
                    if ($orWhere == false) {
                        $query->where('categories.id', '=', intval($category_val));
                        $orWhere = true;
                    } else {
                        $query->orWhere('categories.id', '=', intval($category_val));
                    }
                }
            });
        }

        // Поиск по полю: address
        if( $request->input('address') !== null && strlen($request->input('address')) ) {
            $query->where('services.address', 'like', '%'.$request->input('address').'%');
        }

        $items_total = Service::serviceCount();
        $lastrecord = DB::table('operators')->latest()->first();
        $items_total_query = clone $query;
        $items_found = $items_total_query->get()->count();

        $services = $query->paginate(env('APP_ITEMS_PER_PAGE', 30));
        //echo vsprintf(str_replace(['?'], ['\'%s\''], $query->toSql()), $query->getBindings());

        // Вывод результатов поиска
        return view('services.search.screen')
            ->with('categories', $categories)
            ->with('services', $services)
            ->with('form', $request->all())
            ->with('items_total', $items_total)
            ->with('items_found', $items_found)
            ->with('parsed', $lastrecord);
    }

    /**
     * Функция выводит информацию о выбранном операторе.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @throws \Throwable
     */
    public function show(Request $request, $id)
    {
        // Запрос БД
        $query = Operator::query();

        // Если передена форма запроса, то
        if( $request->isMethod('POST') ) {

            // Если передан идентифиатор оператора, то
            if ( $request->input('id') !== null && intval($request->input('id')) > 0 ) {

                // Составляем запрос в БД по идентификатору оператора
                $query->where('id', '=', intval($request->input('id')));

                // Получае первую найденную запись
                $operator = $query->first();

                // Вормируем json-ответ вэб сервера
                return response()->json(array(
                    'msg' => view('operators.show._operator')->with('operator', $operator)->render()
                ), 200, ['Content-Type' => 'application/json']);
            }
            // Если пользователь зашел по url оператора, то
        } else if( $request->isMethod('GET') ) {

            // Если передан идентифиатор оператора, то
            if ( isset($id)  && $id !== null && intval($id) > 0) {

                // Составляем запрос в БД по идентификатору оператора
                $query->where('id', '=', intval($id));

                // Получае первую найденную запись
                $operator = $query->first();

                // Вормируем ответ вэб сервера
                return view('operators.show.screen')->with('operator', $operator);
            }
        }

        abort(404);
    }
}
