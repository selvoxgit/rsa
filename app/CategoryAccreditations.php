<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryAccreditations extends Model
{
    /**
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     *
     * @var array
     */
    protected $fillable = [
        'category_id',
        'accreditation_id',
    ];
}
