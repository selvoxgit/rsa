<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CategoryServices
 *
 * @package App
 */
class CategoryServices extends Model
{
    /**
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     *
     * @var array
     */
    protected $fillable = [
        'category_id',
        'service_id',
    ];
}
