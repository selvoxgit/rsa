<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inspection extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $dateFormat = 'Y.m.d';

    /**
     * @var array
     */
    protected $dates = ['actdate'];

    /**
     * @var array
     */
    protected $fillable = [
        'operator_id'    ,
        'number'         ,
        'subject'        ,
        'checkdate'      ,
        'actdate'        ,
        'service_address',
        'violations'     ,
        'document'       ,
        'result'         ,
    ];

}
