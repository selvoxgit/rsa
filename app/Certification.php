<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Certification extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $dateFormat = 'Y.m.d';

    /**
     * @var array
     */
    protected $dates = ['orderdate'];

    /**
     * @var array
     */
    protected $fillable = [
        'operator_id',
        'subject'    ,
        'type'       ,
        'orderdate'  ,
        'ordernum'   ,
    ];
}
