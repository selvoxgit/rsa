<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $fillable = [
        'operator_id',
        'rsa_id'     ,
        'address'    ,
        'phone'      ,
    ];

    public $timestamps = false;

    public function categories()
    {
        return $this->belongsToMany(
            'App\Category',
            'category_services',
            'service_id',
            'category_id'
        );
    }

    public function operator()
    {
        return $this->hasOne('App\Operator', 'id');
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return int
     */
    public function scopeServiceCount($query)
    {
        return $query->count('id');
    }
}
