<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    public $timestamps = false;

    public function services()
    {
        return $this->belongsToMany(
            'App\Service',
            'services',
            'category_id',
            'service_id'
        );
    }

    public function accreditations()
    {
        return $this->belongsToMany(
            'App\Accreditation',
            'accreditations',
            'category_id',
            'accreditation_id'

        );
    }

}
