<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('rsa_id')->nullable(false)->index();
            $table->unsignedBigInteger('operator_id')->nullable(false)->index();
            $table->string('address', 500)->nullable(false);
            $table->string('phone', 255);
        });

        Schema::disableForeignKeyConstraints();
            Schema::table('services', function (Blueprint $table) {
                $table->foreign('operator_id')
                    ->references('id')->on('operators')
                    ->onUpdate('cascade')->onDelete('cascade');
            });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
