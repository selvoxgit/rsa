<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInspectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inspections', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('operator_id')->nullable(false)->index();
            $table->unsignedBigInteger('number')->nullable(false);
            $table->text('subject')->nullable(true);
            $table->text('checkdate')->nullable();
            $table->text('actdate')->nullable();
            $table->text('service_address')->nullable();
            $table->text('violations')->nullable();
            $table->text('document')->nullable();
            $table->text('result')->nullable();
        });

        Schema::disableForeignKeyConstraints();
            Schema::table('inspections', function (Blueprint $table) {
                $table->foreign('operator_id')
                    ->references('id')->on('operators')
                    ->onUpdate('cascade')->onDelete('cascade');
            });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inspections');
    }
}
