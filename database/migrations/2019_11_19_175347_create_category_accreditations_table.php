<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoryAccreditationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_accreditations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('category_id')->unsigned()->nullable(false)->default(0);
            $table->bigInteger('accreditation_id')->unsigned()->nullable(false)->default(0);
        });

        Schema::disableForeignKeyConstraints();
            Schema::table('category_accreditations', function (Blueprint $table) {

                $table->foreign('category_id')
                    ->references('id')
                    ->on('categories')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');

                $table->foreign('accreditation_id')
                    ->references('id')
                    ->on('accreditations')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            });
        Schema::enableForeignKeyConstraints();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_accreditations');
    }
}
