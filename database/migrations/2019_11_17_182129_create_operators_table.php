<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOperatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * Table: operators
         * Description: Информация об операторе технического осмотра
         * *************+***************+***************************************************************
         * Field        | Type          | Description
         * *************+***************+***************************************************************
         * id           | bigint(20)    | Внутренний идентификатор оператора технического осмотра (ОТО)
         * rsa_id       | bigint(20)    | Идентификатор записи по БД РСА
         * status       | varchar(255)  | Статус оператора
         * shortname    | varchar(255)  | Краткое наименование организации ОТО
         * fullname     | varchar(255)  | Полное наименование органиазции ОТО
         * manager      | varchar(100)  | ФИО Директора организации ОТО
         * ogrn         | bigint(20)    | Основной государственный регистрационный номер ОТО
         * address      | varchar(255)  | Адрес местонахождения ОТО
         * phone        | varchar(50)   | Телефон для связи с ОТО
         * email        | varchar(255)  | Адрес эл. почты ОТО
         * *************+***************+***************************************************************
         */
        Schema::create('operators', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('rsa_id')->unique();
            $table->unsignedBigInteger('status_id')->nullable(false)->index();
            $table->string('shortname', 255);
            $table->string('fullname', 255)->nullable();
            $table->string('manager', 100);
            $table->unsignedBigInteger('ogrn');
            $table->string('address', 255)->nullable();
            $table->string('phone', 255)->nullable();
            $table->string('email', 255)->nullable();
            $table->timestamps();
        });

        Schema::disableForeignKeyConstraints();
            Schema::table('operators', function (Blueprint $table) {
                $table->foreign('status_id')
                    ->references('id')
                    ->on('statuses')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('operators');
    }
}
