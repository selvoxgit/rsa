<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCertificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('certifications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('operator_id')->nullable(false)->index();
            $table->text('subject')->nullable(true)->default(null);
            $table->string('type', 255)->nullable(true)->default(null);
            $table->date('orderdate')->nullable(true)->default(null);
            $table->string('ordernum', 255)->nullable(true)->default(null);
        });

        Schema::disableForeignKeyConstraints();
            Schema::table('certifications', function (Blueprint $table) {
                $table->foreign('operator_id')
                    ->references('id')->on('operators')
                    ->onUpdate('cascade')->onDelete('cascade');
            });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('certifications');
    }
}
