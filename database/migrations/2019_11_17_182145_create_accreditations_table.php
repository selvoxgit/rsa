<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccreditationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accreditations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('operator_id')->index();
            $table->string('type', 255)->nullable(false);
            $table->unsignedBigInteger('certnum')->nullable();
            $table->date('orderdate')->nullable(true)->default(null);
            $table->string('ordernum', 50)->nullable(true)->default(null);
            $table->unsignedBigInteger('services')->default(0);
        });

        Schema::disableForeignKeyConstraints();
            Schema::table('accreditations', function (Blueprint $table) {
                $table->foreign('operator_id')
                    ->references('id')
                    ->on('operators')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accreditations');
    }
}
