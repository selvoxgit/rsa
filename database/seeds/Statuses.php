<?php

use Illuminate\Database\Seeder;

class Statuses extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('statuses')->insert(['name' => 'N/A',        'icon' => 'fas fa-question text-secondary']);
        DB::table('statuses')->insert(['name' => 'Valid',      'icon' => 'fas fa-check text-success']);
        DB::table('statuses')->insert(['name' => 'Suspended',  'icon' => 'fas fa-pause text-info']);
        DB::table('statuses')->insert(['name' => 'Invalidate', 'icon' => 'fas fa-times text-danger']);
    }
}
