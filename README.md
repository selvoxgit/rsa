#Requirements for Production
To run Application we recommend your host supports the following:
1. Unix or Linux system
2. PHP version 7.2.x
    * BCMath PHP Extension
    * Ctype PHP Extension
    * JSON PHP Extension
    * MbString PHP Extension
    * OpenSSL PHP Extension
    * PDO PHP Extension
    * Tokenizer PHP Extension
    * XML PHP Extension
3. Laravel 6.x and Laravel Tinker 1.x
4. fideloper/proxy ^4.x
5. GuzzleHttp ^6.4
6. Composer 6.13.x
7. npm or yarn
8. MySQL version 5.6+ or MariaDB version 10.0+
9. HTTPS support for Web Server
10. We recommend Apache and NGINX as the most robust servers with the most features for running WordPress, but any server that supports PHP and MySQL will do.

#Requirements for Development
1. All requirements for production version
2. facade/ignition" ^1.4
3. fzaninotto/faker ^1.4
4. laravel/ui ^1.1
5. mockery/mockery ^1.0
6. nunomaduro/collision ^3.0
7. phpunit/phpunit ^8.0
8. xdebug
9. @fortawesome/fontawesome-free ^5.11.2
10. @mdi/font ^4.5.95
11. axios ^0.19
12. bootstrap ^4.0.0
13. bootstrap-select ^1.13.12
14. cross-env ^5.1
15. datatables.net-bs4 ^1.10.20
16. datatables.net-fixedheader-bs4 ^3.1.6
17. datatables.net-select-bs4 ^1.3.1
18. jquery ^3.2
19. laravel-mix ^4.0.7
20. lodash ^4.17.13
21. moment ^2.24.0
22. moment-timezone ^0.5.27
23. popper.js ^1.12.x
24. resolve-url-loader ^2.3.x
25. roboto-fontface ^0.10.x
26. sass ^1.15.x
27. sass-loader ^7.1.x
28. tempusdominus-bootstrap-4 ^5.1.x
29. tempusdominus-core ^5.0.x
30. vue-template-compiler ^2.6.x

#Install
1. Create new empty directory
```mkdir ~/YOUR_EMPTY_DIRECTORY```
2. Clone git repository with command
```git clone https://skhalipa@bitbucket.org/skhalipa/rsa.git```
3. If you using Apache, you can copy .htaccess.example by command 
```cp ./.htaccess.example ./.htaccess```
4. Don't forget install dependencies with command
```npm install```
5. Copy example environments with commands ``` cp ./env.example ./.env``` and change it for your business tasks
6. Install database skeleton by typing command ```php artisan migrate:fresh --seed```
7. Clear & cache config files (dont't forget item 5 before do that) by typing ```php artisan config:clear && php artisan config:cache```

#Parse RSA Database
1. You can parse entire RSA site with command ```php artisan rsa:parse```

#Compile sources
1. If you change styles, javascripts you must execute ```npm run development``` for development version and for production version ```npm run production```
